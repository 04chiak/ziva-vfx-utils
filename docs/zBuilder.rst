zBuilder
========

zBuilder saves the current state of the ziva setup and allows you to re-build
scene among other things.  For example searching and replacing and using that
to mirror setups.  The basic idea is to build a ziva setup by hand and use these 
scripts to save it.



.. include:: zBuilder_tutorials.include.rst